#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <unistd.h>

void print_processs_info(char *process_name);
void print_status_info(int status);

int main(void) {
    print_processs_info("current");

    switch (fork()) {
        case -1:
            perror("fork");
            exit(1);

        case 0:
            for (int i = 0; i < 3; i += 1) {
                print_processs_info("child"); 
            }
            exit(0);

        default:
            for (int i = 0; i < 3; i += 1) {
                print_processs_info("parent");
            } 
            int status;
            if (wait(&status) == -1) {
                perror("wait() status");
            }
            print_status_info(status);
            break;
    }
    printf("\nParent process is ending");
    exit(0);
}

void print_processs_info(char *process_name) {
    pid_t pid = getpid();
    printf("\nThe %s process ID is %d", process_name, pid);

    pid_t gpid = getgid();
    printf("\nThe %s process group ID is %d", process_name, gpid);

    pid_t spid = getsid(pid);
    printf("\nThe %s process session ID is %d", process_name, spid);
}

void print_status_info(int status) {
    if (WIFEXITED(status)) {
        printf("\nThe child has exited with status of %d\n", WEXITSTATUS(status));
    }
    else {
        printf("\nThe child has not exited successfully");
    }
}