#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>
#include <stdarg.h>
#include <sys/stat.h>

void write_to_log(int log_fd, char *message);
void print_to_buffer_formatted_string(char *buffer, const char *format, ...);
void write_process_info_to_buffer(char *buffer);
void write_process_info_to_log(int log_fd);

int open_for_writing(const char *filename);

void close_all_fds();
void redirect_standard_fds(void);

int main(void) {
    pid_t pid, sid;
    int log_fd = open_for_writing("log.txt");
    write_to_log(log_fd, (char *)"\nThe process has started");
    
    pid = fork();
    if (pid < 0) {
        exit(EXIT_FAILURE);
    }

    if (pid > 0) {
        write_process_info_to_log(log_fd);
        exit(EXIT_SUCCESS);
    }

    umask(0);
            
    sid = setsid();
    if (sid < 0) {
        exit(EXIT_FAILURE);
    }
    
    if ((chdir("./root")) < 0) {
        exit(EXIT_FAILURE);
    }
    
    close_all_fds();
    redirect_standard_fds();

    log_fd = open_for_writing("daemon_log.txt");

    while (1) {
        write_process_info_to_log(log_fd);
        sleep(5);
    }

    close(log_fd);
    exit(EXIT_SUCCESS);
}

void write_process_info_to_log(int log_fd) {
    char buffer[512];
    write_process_info_to_buffer(buffer);
    write_to_log(log_fd, buffer);
}


int open_for_writing(const char *filename) {
    int fd = open(filename, O_WRONLY | O_APPEND | O_CREAT, 0777);
    if (fd == -1) {
        exit(EXIT_FAILURE);
    }
    return fd;
}

void write_to_log(int log_fd, char *message) {
    if (write(log_fd, message, strlen(message)) == -1) {
        write(log_fd, strerror(errno), strlen(strerror(errno)));
        exit(EXIT_FAILURE);
    }
}

void close_all_fds() {
    int max_fd = sysconf(_SC_OPEN_MAX); // getdtablesize();
    for (int i = 0; i < max_fd; ++i) {
        close(i);
    }
}

void redirect_standard_fds(void) {
    // 0 - stdin, 1 - stdout, 2 - stderr
    char DEV_NULL[10] = "/dev/null";
    if (open(DEV_NULL, O_RDONLY) == -1 || 
    open(DEV_NULL, O_WRONLY) == -1 || 
    open(DEV_NULL, O_WRONLY) == -1) {
        exit(EXIT_FAILURE);
    }
}

void print_to_buffer_formatted_string(char *buffer, const char *format, ...) {
  va_list args;
  va_start(args, format);
  vsprintf(buffer,format, args);
  va_end(args);
}

void write_process_info_to_buffer(char *buffer) {
    pid_t pid = getpid();
    pid_t gpid = getgid();
    pid_t spid = getsid(pid);
    print_to_buffer_formatted_string(
        buffer, 
        "\nProcess ID: %d, process group ID: %d, process session ID: %d", 
        pid, gpid, spid
    );
}