CC=gcc
CFLAGS=-Wall -Werror -g -ggdb
SRC=src

task1: subprogram_1.o
	$(CC) $(CFLAGS) subprogram_1.o -o task1

task2: subprogram_2.o
	$(CC) $(CFLAGS) subprogram_2.o -o task2

subprogram_1.o: $(SRC)/subprogram_1.c
	$(CC) $(CFLAGS) -c $(SRC)/subprogram_1.c

subprogram_2.o: $(SRC)/subprogram_2.c
	$(CC) $(CFLAGS) -c $(SRC)/subprogram_2.c

clean:
	rm -rf *.o task1 task2