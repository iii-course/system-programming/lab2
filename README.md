# lab2

## Description
This is an education project written in C. It consists of 2 subprograms. The first one prints its process parameters (getpid(), getgid(), getsid()) and uses fork(). The second one creates a daemon. 

## Installation
To compile each program, simply run one of the following commands:
```sh
make task1
make task2
```

## Usage
### Run
To run the program, run one of the following commands:
```sh
./task1
./task2
```

To clean all *.o files, run:
```sh
make clean
```

### Kill daemon
To kill the daemon created in task2, run this commands:
```sh
ps -xj | grep task2
kill -9 pid
```

### Log
You can find log data for task2 in these files: ./log.txt (for parent process), ./root/daemon_log.txt (for daemon).